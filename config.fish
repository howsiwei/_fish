prepend_path $HOME/.cargo/bin
set SHELL (which fish)
if status -i
	set __fish_vi_mode 1
	set fish_key_bindings fish_vi_key_bindings
	set fish_color_search_match --background=brgreen
	set -x FZF_DEFAULT_COMMAND 'rg --files --hidden'
	set -x FZF_CTRL_T_COMMAND $FZF_DEFAULT_COMMAND
	set -x VIM_CONFIG_HOME "$XDG_CONFIG_HOME/nvim"
	set -x VIM_DATA_HOME "$XDG_DATA_HOME/nvim"
	set -x CXX 'g++-8'
	set -x CXXFLAGS '--std=c++14 -Wall -Wextra -Wno-parentheses'
	set -x MPD_PORT 6600
	set -x TODO_DIR "$XDG_DATA_HOME/todo"
	set -x TODO_FILE "$TODO_DIR/todo.txt"
	set -x DONE_FILE "$TODO_DIR/done.txt"
	set -x REPORT_FILE "$TODO_DIR/report.txt"
	alias e nvim
	alias g git
	alias gh hub
	alias l ls
	alias o open
	set -x VIRTUALFISH_HOME "$XDG_DATA_HOME/virtualenvs"
	eval (python3 -m virtualfish auto_activation)
	alias t todo.sh
	alias v vf
	alias coqide /Applications/CoqIDE_8.8.0.app/Contents/MacOS/coqide
end

# emacs ansi-term support
if test -n "$EMACS"
  set -x TERM eterm-color
end

# this function may be required
function fish_title
  true
end
