function mcd --description 'Make and change directory'
	command mkdir $argv
	if test $status = 0
		set -l dir $argv[(count $argv)]
		switch $dir
		case '--'
		case '*'
			cd $dir
		end
	end
end
