function fish_user_key_bindings
	fzf_key_bindings
	bind \co up-or-search
	bind -M insert \co up-or-search
	bind -M insert \cn down-or-search
	bind -M insert \ca beginning-of-line
	bind -M insert \ce end-of-line
end

