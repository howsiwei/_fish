function transfer
	if test (count $argv) -eq 0
		echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"
		return 1
	end

	## upload stdin or file
	set file $argv[1]

	## get temporarily filename, output is written to this file show progress can be showed
	set tmpdir (mktemp -d "/tmp/transferXXXX")
	# echo $tmpdir
	set linkfile "$tmpdir/link.txt"

	if test -t 0
		if not test -r $file
			if not test -e $file
				echo "File $file doesn't exists."
			else
				echo "File $file is not readable."
			end
			return 1
		end

		set base (basename $file)
		set slug (echo -n $base | sed -e 's/[^a-zA-Z0-9._-]/-/g')

		if test -d $file
			# zip directory and transfer
			set zipfile "$tmpdir/transfer.zip"
			pushd (dirname $file)
			zip -qr $zipfile $base
			popd
			curl --progress-bar --upload-file $zipfile "https://transfer.sh/$slug.zip" >> $linkfile
			rm -f $zipfile
		else
			# transfer file
			curl --progress-bar --upload-file $file "https://transfer.sh/$slug" >> $linkfile
		end

	else
		# transfer pipe
		curl --progress-bar --upload-file '-' "https://transfer.sh/$file" >> $linkfile
	end

	## cat output link
	cat $linkfile | clip -i
	cat $linkfile

	## cleanup
	rm -rf $tmpdir
end
