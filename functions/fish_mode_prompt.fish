function fish_mode_prompt --description 'Displays the current mode'
	# Do nothing if not in vi mode
	if test $fish_key_bindings = 'fish_vi_key_bindings'
		switch $fish_bind_mode
		case default
			# set_color --bold --background red white
			# echo '[N]'
			set_color white
			echo ':'
		case insert
			# set_color --bold --background green white
			# echo '[I]'
			set_color normal
			echo '+'
		case replace-one
			# set_color --bold --background green white
			# echo '[R]'
			set_color normal
			echo '%'
		case visual
			# set_color --bold --background magenta white
			# echo '[V]'
			set_color magenta
			echo '|'
		end
		if set -q VIRTUAL_ENV
			echo -n -s (set_color -b black blue) "(" (basename "$VIRTUAL_ENV") ")"
end
		set_color normal
		# echo -n ' '
	end
end

