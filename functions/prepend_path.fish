function prepend_path
	for v in $argv[-1..1]
		if not contains $v $PATH
			set PATH $v $PATH
		end
	end
	set -x PATH $PATH
end
